package org.abbtech.exercise15;

import org.abbtech.exercise14.CalculatorService;

public class CalculatorServiceImpl implements CalculatorService {
    @Override
    public double addition(double a, double b) {
        return a + b;
    }

    @Override
    public double subtract(double a, double b) {
        return a - b;
    }

    @Override
    public double multiply(double a, double b) {
        return a * b;
    }

    @Override
    public double division(double a, double b) {
        return a / b;
    }
}