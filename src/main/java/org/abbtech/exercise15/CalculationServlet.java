//package org.abbtech.exercise15;
//
//import jakarta.servlet.ServletException;
//import jakarta.servlet.ServletRequest;
//import jakarta.servlet.ServletResponse;
//import jakarta.servlet.annotation.WebServlet;
//import jakarta.servlet.http.HttpServlet;
//import jakarta.servlet.http.HttpServletRequest;
//import jakarta.servlet.http.HttpServletResponse;
//
//import java.io.IOException;
//import java.io.PrintWriter;
//import java.sql.SQLException;
//import java.util.List;
//import java.util.stream.Collectors;
//
//@WebServlet(name = "CalculationServlet", urlPatterns = {"/calculator/add", "/calculator/subtract", "/calculator/multiply", "/calculator/division"})
//public class CalculationServlet extends HttpServlet {
//    private transient ApplicationService applicationService;
//    private CalculationRepository calculationRepository;
//    private static final String OUTPUT_FORMAT = "application/json";
//
//    @Override
//    public void init() throws ServletException {
//        System.out.println("init() method called");
//        super.init();
//        applicationService = new ApplicationService(new CalculatorServiceImpl());
//        calculationRepository = new CalculationRepository();
//    }
//
//    @Override
//    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
//        System.out.println("service() method called");
//        super.service(req, res);
//    }
//
//    @Override
//    public void destroy() {
//        System.out.println("destroy() method called");
//        super.destroy();
//    }
//
//    // CREATE & READ
//    @Override
//    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
//        String calculationMethod = req.getHeader("x-calculation-method");
//        double a = Double.parseDouble(req.getParameter("a"));
//        double b = Double.parseDouble(req.getParameter("b"));
//        int id = Integer.parseInt(req.getParameter("id"));
//        double result;
//
//        PrintWriter writer = resp.getWriter();
//
//        try {
//            result = callMethods(calculationMethod, a, b);
//
//            // I assumed that parameters will be given correct.
//            calculationRepository.saveCalculationResult(id, a, b, result, calculationMethod); // create
//
//            var calculations = calculationRepository.getCalculationResult(); // read
//
//            resp.setContentType(OUTPUT_FORMAT);
//            writer.write(normalResponse(result, calculations));
//        } catch (ArithmeticException | InvalidMethodException e) {
//            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
//            writer.write(exceptionResponse(e.getMessage()));
//        } catch (SQLException e) {
//            resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
//            writer.write(exceptionResponse(e.getMessage()));
//        } finally {
//            writer.close();
//        }
//    }
//
//    // UPDATE
//    @Override
//    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws IOException {
//        String calculationMethod = req.getHeader("x-calculation-method");
//        int a = Integer.parseInt(req.getParameter("a"));
//        int b = Integer.parseInt(req.getParameter("b"));
//        int id = Integer.parseInt(req.getParameter("id"));
//        double result;
//        PrintWriter writer = resp.getWriter();
//
//        try {
//            result = callMethods(calculationMethod, a, b);
//
//            calculationRepository.updateCalculationResult(a, b, result, id);
//
//            resp.setContentType(OUTPUT_FORMAT);
//            writer.write(normalResponse(result, null));
//        } catch (ArithmeticException | InvalidMethodException e) {
//            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
//            writer.write(exceptionResponse(e.getMessage()));
//        } catch (SQLException e) {
//            resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
//            writer.write(exceptionResponse(e.getMessage()));
//        }
//
//        writer.close();
//    }
//
//    // DELETE
//    @Override
//    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws IOException {
//        int id = Integer.parseInt(req.getParameter("id"));
//        PrintWriter writer = resp.getWriter();
//
//        try {
//            calculationRepository.deleteCalculationResult(id);
//
//            resp.setContentType(OUTPUT_FORMAT);
//            writer.write("""
//                {
//                    "result":"ok"
//                }
//                """);
//        } catch (SQLException e) {
//            writer.write(exceptionResponse(e.getMessage()));
//        }
//
//        writer.close();
//    }
//
//    String normalResponse(double result, List<CalculationResultDTO> calculationResultDTOList) {
//        String textBlock;
//        String listResult;
//
//        if (calculationResultDTOList != null) {
//            listResult = "[" + calculationResultDTOList.stream()
//                    .map(Record::toString)
//                    .collect(Collectors.joining(", ")) + "]";
//
//            textBlock = """
//               {
//                    "result":""" + result + ',' + """
//                    "list": %s
//                }
//            """.formatted(listResult);
//        } else {
//            textBlock = """
//               {
//                    "result":""" + result + """
//                }
//            """;
//        }
//
//        return """
//                    %s
//               """.formatted(textBlock);
//    }
//
//    String exceptionResponse(String message) {
//        return """
//                {
//                    "exception": "%s"
//                }
//                """.formatted(message);
//    }
//
//    double callMethods(String calculationMethod, double a, double b) throws InvalidMethodException {
//        if (calculationMethod == null) {
//            throw new InvalidMethodException("Add x-calculation-method header");
//        }
//
//        return switch (calculationMethod.toLowerCase()) {
//            case "add" -> applicationService.addition(a, b);
//            case "subtract" -> applicationService.subtract(a, b);
//            case "multiply" -> applicationService.multiply(a, b);
//            case "division" -> applicationService.division(a, b);
//            default -> throw new InvalidMethodException("Only add, subtract, multiply, and division methods are accepted.");
//        };
//    }
//}