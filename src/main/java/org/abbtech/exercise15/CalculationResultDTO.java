package org.abbtech.exercise15;

public record CalculationResultDTO(double a, double b, double result, String method) {

}