package org.abbtech.exercise15;

public class InvalidMethodException extends Exception {
    public InvalidMethodException(String message) {
        super(message);
    }
}
