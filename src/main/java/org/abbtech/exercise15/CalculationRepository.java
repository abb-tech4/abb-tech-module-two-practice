package org.abbtech.exercise15;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CalculationRepository {
    private static final String JDBC_URL = "jdbc:postgresql://localhost:5432/abb_tech";
    private static final String USER_NAME = "postgres";
    private static final String PASS = "apricity78";

    // CREATE
    public void saveCalculationResult(int id, double a, double b, double result, String method) throws SQLException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        String sqlInsert = """
                INSERT INTO "calculation".calculation_result(id, variable_a, variable_b, calc_result, calc_method)
                VALUES (?,?,?,?,?)
                """;

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
       }

        try {
            connection = DriverManager.getConnection(JDBC_URL, USER_NAME, PASS);
            connection.setAutoCommit(false);

            preparedStatement = connection.prepareStatement(sqlInsert);
            preparedStatement.setDouble(1, id);
            preparedStatement.setDouble(2, a);
            preparedStatement.setDouble(3, b);
            preparedStatement.setDouble(4, result);
            preparedStatement.setString(5, method);
            preparedStatement.execute();

            connection.commit();
        } catch (SQLException e) {
            try {
                if (connection != null) {
                    connection.rollback();
                }
            } catch (SQLException ex) {
                throw new SQLException(ex);
            }
        } finally {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        }
    }

    public List<CalculationResultDTO> getCalculationResult() throws SQLException {
        Connection connection = null;
        List<CalculationResultDTO> calculationResultDTOS = new ArrayList<>();
        String sqlSelect = """
                SELECT * FROM "calculation".calculation_result;
                """;

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        try {
            connection = DriverManager.getConnection(JDBC_URL, USER_NAME, PASS);
            connection.setAutoCommit(false);

            PreparedStatement preparedStatement = connection.prepareStatement(sqlSelect);
            ResultSet calculationResults = preparedStatement.executeQuery();

            while (calculationResults.next()) {
                double a = calculationResults.getDouble("VARIABLE_A");
                double b = calculationResults.getDouble("VARIABLE_B");
                double result = calculationResults.getDouble("CALC_RESULT");
                String calcMethod = calculationResults.getString("CALC_METHOD");
                calculationResultDTOS.add(new CalculationResultDTO(a, b, result, calcMethod));
            }
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                throw new SQLException(ex);
            }
        } finally {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        }

        return calculationResultDTOS;
    }

    // UPDATE
    public void updateCalculationResult(double a, double b, double result, int id) throws SQLException {
        Connection connection = null;
        String sqlUpdate = """
                UPDATE "calculation".calculation_result SET variable_a = ?, variable_b = ?, calc_result = ? WHERE id = ?;
                """;

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        try {
            connection = DriverManager.getConnection(JDBC_URL, USER_NAME, PASS);
            connection.setAutoCommit(false);

            PreparedStatement preparedStatement = connection.prepareStatement(sqlUpdate);
            preparedStatement.setDouble(1, a);
            preparedStatement.setDouble(2, b);
            preparedStatement.setDouble(3, result);
            preparedStatement.setDouble(4, id);
            preparedStatement.execute();

            connection.commit();
        } catch (NullPointerException | SQLException e) {
            try {
                if (connection != null) {
                    connection.rollback();
                }
            } catch (NullPointerException ex) {
                throw new NullPointerException();
            }
        } finally {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        }
    }

    // DELETE
    public void deleteCalculationResult(double id) throws SQLException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        String sqlDelete = """
                DELETE FROM "calculation".calculation_result WHERE id = ?;
                """;

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        try {
            connection = DriverManager.getConnection(JDBC_URL, USER_NAME, PASS);
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(sqlDelete);
            preparedStatement.setDouble(1, id);
            preparedStatement.execute();
            connection.commit();
        } catch (SQLException e) {
            try {
                if (connection != null) {
                    connection.rollback();
                }
            } catch (SQLException ex) {
                throw new SQLException(ex);
            }
        } finally {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        }
    }
}