package org.abbtech.exam;

public record UserDTO(String username, String email, String hashedPassword) {
}
