package org.abbtech.exam;

import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

@WebServlet(name = "UserLoginServlet", urlPatterns = {"/user/login"})
public class UserLoginServlet extends HttpServlet {
    private transient UserRepository userRepository;

    @Override
    public void init() throws ServletException {
        super.init();
        System.out.println("init() method called");
        userRepository = new UserRepository();
    }

    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        super.service(req, res);
        System.out.println("service() method called");
    }

    @Override
    public void destroy() {
        super.destroy();
        System.out.println("destroy() method called");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter writer = resp.getWriter();
        String actualUsername = req.getParameter("username");
        String actualPass = req.getParameter("pass");
        String actualEmail = req.getParameter("email");
        try {
            UserDTO user = UserRepository.findByUsername(actualUsername, userRepository.getUsers());

            if (user != null && user.username().equalsIgnoreCase(actualUsername) &&
                    UserRepository.checkPassword(actualPass, user.hashedPassword()) &&
                    user.email().equalsIgnoreCase(actualEmail)) {
                HttpSession httpSession = req.getSession();
                httpSession.setAttribute("username", actualUsername);
                httpSession.setAttribute("user-role", "student-role");

                Cookie cookieUser = new Cookie("username", actualUsername);
                Cookie cookieUserRole = new Cookie("user-role", "student-role");
                cookieUser.setMaxAge(3000);
                cookieUserRole.setMaxAge(3000);
                resp.addCookie(cookieUser);
                resp.addCookie(cookieUserRole);

                writer.write("User logged in  with username: " + actualUsername);
            } else {
                writer.write("Could not log in.");
            }
        } catch (SQLException | ClassNotFoundException e) {
            resp.setStatus(500);
            resp.getWriter().write("Server problem.");
        }
    }
}