package org.abbtech.exam;

import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

@WebServlet(name = "UserRegistrationServlet", urlPatterns = {"/user/register"})
public class UserRegistrationServlet extends HttpServlet {
    private transient UserRepository userRepository;

    @Override
    public void init() throws ServletException {
        super.init();
        System.out.println("init() method called");
        userRepository = new UserRepository();
    }

    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        super.service(req, res);
        System.out.println("service() method called");
    }

    @Override
    public void destroy() {
        super.destroy();
        System.out.println("destroy() method called");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter writer = resp.getWriter();
        String actualUsername = req.getParameter("username");
        String actualPass = req.getParameter("pass");
        String actualEmail = req.getParameter("email");

        // if an account with the same username is not created
        try {
            if (UserRepository.findByUsername(actualUsername, userRepository.getUsers()) == null) {
                String hash = UserRepository.hashPassword(actualPass);

                userRepository.createUser(actualUsername, actualEmail, hash);

                writer.write("Welcome, " + actualUsername + "! You registered successfully.");
            } else {
                writer.write("The user with the name " + actualUsername + " already exists.");
            }
        } catch (SQLException | ClassNotFoundException e) {
            resp.setStatus(500);
            resp.getWriter().write("Server problem.");
        }
    }
}