package org.abbtech.exam;

import org.mindrot.jbcrypt.BCrypt;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserRepository {
    private static final String JDBC_URL = "jdbc:postgresql://localhost:5432/abb_tech";
    private static final String USER_NAME = "postgres";
    private static final String PASS = "apricity78";

    // CREATE
    public void createUser(String username, String email, String hashedPassword) throws SQLException, ClassNotFoundException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        String sqlInsert = """
                INSERT INTO "public".users(username, email, hashed_password)
                VALUES (?,?,?)
                """;

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new ClassNotFoundException("Driver is not found.");
        }

        try {
            connection = DriverManager.getConnection(JDBC_URL, USER_NAME, PASS);
            connection.setAutoCommit(false);

            preparedStatement = connection.prepareStatement(sqlInsert);
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, email);
            preparedStatement.setString(3, hashedPassword);
            preparedStatement.execute();

            connection.commit();
        } catch (SQLException e) {
            try {
                if (connection != null) {
                    connection.rollback();
                }
            } catch (SQLException ex) {
                throw new SQLException("Connection is failed.");
            }
        } finally {
            assert connection != null;
            connection.close();

            assert preparedStatement != null;
            preparedStatement.close();
        }
    }

    // READ
    public List<UserDTO> getUsers() throws SQLException, ClassNotFoundException {
        Connection connection = null;
        List<UserDTO> userDTOS = new ArrayList<>();
        String sqlSelect = """
                SELECT * FROM "public".users;
                """;

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new ClassNotFoundException("Driver is not found.");
        }

        try {
            connection = DriverManager.getConnection(JDBC_URL, USER_NAME, PASS);
            connection.setAutoCommit(false);

            PreparedStatement preparedStatement = connection.prepareStatement(sqlSelect);
            ResultSet users = preparedStatement.executeQuery();

            while (users.next()) {
                String username = users.getString("username");
                String email = users.getString("email");
                String hashedPassword = users.getString("hashed_password");
                userDTOS.add(new UserDTO(username, email, hashedPassword));
            }
        } catch (SQLException e) {
            try {
                if (connection != null) {
                    connection.rollback();
                }
            } catch (SQLException ex) {
                throw new SQLException("Connection is failed.");
            }
        } finally {
            assert connection != null;
            connection.close();
        }

        return userDTOS;
    }

    public static UserDTO findByUsername(String username, List<UserDTO> users) {
        for (UserDTO u : users) {
            if (u.username().equals(username)) {
                return u;
            }
        }

        return null;
    }

    public static String hashPassword(String password) {
        return BCrypt.hashpw(password, BCrypt.gensalt());
    }

    public static boolean checkPassword(String password, String hashedPassword) {
        return BCrypt.checkpw(password, hashedPassword);
    }

    // UPDATE
    public void updateAssignment(String username, String level, int grade) throws SQLException {
        Connection connection = null;
        String sqlUpdate = """
                UPDATE "public".assignments SET level_ = ?, grade = ? WHERE username = ?
                """;

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        try {
            connection = DriverManager.getConnection(JDBC_URL, USER_NAME, PASS);
            connection.setAutoCommit(false);

            PreparedStatement preparedStatement = connection.prepareStatement(sqlUpdate);
            preparedStatement.setString(1, level);
            preparedStatement.setInt(2, grade);
            preparedStatement.setString(3, username);
            preparedStatement.execute();

            connection.commit();
        } catch (NullPointerException | SQLException e) {
            try {
                if (connection != null) {
                    connection.rollback();
                }
            } catch (NullPointerException ex) {
                throw new NullPointerException();
            }
        } finally {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        }
    }

    // DELETE
    public void deleteAssignment(String username) throws SQLException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        String sqlDelete = """
                DELETE FROM "public".assignments WHERE username = ?;
                """;

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        try {
            connection = DriverManager.getConnection(JDBC_URL, USER_NAME, PASS);
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(sqlDelete);
            preparedStatement.setString(1, username);
            preparedStatement.execute();
            connection.commit();
        } catch (SQLException e) {
            try {
                if (connection != null) {
                    connection.rollback();
                }
            } catch (SQLException ex) {
                throw new SQLException(ex);
            }
        } finally {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        }
    }
}