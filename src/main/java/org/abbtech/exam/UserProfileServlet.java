package org.abbtech.exam;

import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.sql.SQLException;

@WebServlet(name = "UserProfileServlet", urlPatterns = {"/user/profile"})
public class UserProfileServlet extends HttpServlet {
    private UserRepository userRepository;

    @Override
    public void init() throws ServletException {
        super.init();
        userRepository = new UserRepository();
    }

    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        super.service(req, res);
    }

    @Override
    public void destroy() {
        super.destroy();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        var writer = resp.getWriter();
        String username = SecurityUtil.getCookieByName(req.getCookies(), "username").getValue();
        writer.write("User Profile: " + username);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        var writer = resp.getWriter();

        String username = SecurityUtil.getCookieByName(req.getCookies(), "username").getValue();
        String level = req.getParameter("level");
        int grade = Integer.parseInt(req.getParameter("grade"));

        try {
            userRepository.updateAssignment(username, level, grade);
        } catch (SQLException e) {
            resp.setStatus(500);
            resp.getWriter().write("Server problem.");
        }

        writer.write("Assignment updated");
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        var writer = resp.getWriter();

        String username = SecurityUtil.getCookieByName(req.getCookies(), "username").getValue();

        try {
            userRepository.deleteAssignment(username);
        } catch (SQLException e) {
            resp.setStatus(500);
            resp.getWriter().write("Server problem.");
        }

        writer.write("Assignment deleted");
    }
}