package org.abbtech.lesson2;

import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.*;
import org.abbtech.lesson3.CalculationRepository;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

@WebServlet(name = "CalculationServlet", urlPatterns = {"/calculator/add", "/calculator/subtract", "/calculator/multiply"})
public class CalculationServlet extends HttpServlet {
    private ApplicationService applicationService;
    private CalculationRepository calculationRepository;

    @Override
    public void init() throws ServletException {
        super.init();
        applicationService = new ApplicationService(new CalculatorServiceImpl());
        calculationRepository = new CalculationRepository();
    }

    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        super.service(req, res);
    }

    @Override
    public void destroy() {
        super.destroy();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        String calculationMethod = "multiply";//req.getHeader("x-calculation-method");
        int a = Integer.parseInt(req.getParameter("a"));
        int b = Integer.parseInt(req.getParameter("b"));
        int result = 0;
        PrintWriter writer = resp.getWriter();
        String customAttribute = (String) req.getAttribute("x-filter-name");
        HttpSession httpSession = req.getSession();
//        httpSession.getAttribute("session-use");
        httpSession.setAttribute("session-use", "session-user");
        String string = httpSession.getId();

        var cks = req.getCookies();
        String userDefined = cks[0].getValue();


        try {
            if (calculationMethod.equalsIgnoreCase("multiply")) {
                result = applicationService.multiply(a, b);
            }

//            calculationRepository.saveCalculationResult(a, b, result, calculationMethod);
//            var calculations = calculationRepository.getCalculationResult();
//            resp.setContentType("application/json");
            writer.write("""
                {
                    "result":"ok"
                }
                """);

            Cookie cookieUser = new Cookie("user-name", "guest");
            Cookie cookieUserRole = new Cookie("user-role", "guest-role");
            cookieUserRole.setMaxAge(300);
            cookieUser.setMaxAge(300);
            resp.addCookie(cookieUser);
            resp.addCookie(cookieUserRole);

        } catch (ArithmeticException arithmeticException) {
            // you can give argument ranges
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            writer.write("""
                    {
                        "exception": "INVALID ARGUMENTS"
                    }
                    """);
        }

//        writer.close();
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        BufferedReader bufferedReader = req.getReader();
        PrintWriter writer = resp.getWriter();
        int lineLine;
        while ((lineLine = bufferedReader.read()) != -1) {
            char chr = (char) lineLine;
            writer.write(chr);
        }
        writer.close();
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int id = Integer.parseInt(req.getParameter("id"));
        int result = 1;
        PrintWriter writer = resp.getWriter();

        try {
            calculationRepository.deleteCalculationResult(id);
            resp.setContentType("application/json");
            writer.write("""
                {
                    "result":"ok"
                }
                """);
        } catch (Exception exception) {

        }

        writer.close();
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String calculationMethod = req.getHeader("x-calculation-method");
        int a = Integer.parseInt(req.getParameter("a"));
        int b = Integer.parseInt(req.getParameter("b"));
        int id = Integer.parseInt(req.getParameter("id"));
        int result = 0;
        PrintWriter writer = resp.getWriter();

        try {
            calculationRepository.updateCalculationResult(a, b, id);
            resp.setContentType("application/json");
            writer.write("""
                {
                    "result":"ok"
                }
                """);
        } catch (ArithmeticException arithmeticException) {
            // you can give argument ranges
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            writer.write("""
                    {
                        "exception": "INVALID ARGUMENTS"
                    }
                    """);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        writer.close();
    }
}