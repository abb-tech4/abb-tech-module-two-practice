//package org.abbtech.lesson3;
//
//import jakarta.servlet.ServletException;
//import jakarta.servlet.annotation.WebServlet;
//import jakarta.servlet.http.HttpServlet;
//import jakarta.servlet.http.HttpServletRequest;
//import jakarta.servlet.http.HttpServletResponse;
//
//import java.io.IOException;
//import java.io.PrintWriter;
//import java.sql.Connection;
//import java.sql.DriverManager;
//import java.sql.SQLException;
//
//@WebServlet("/testdb")
//public class TestDatabaseServlet extends HttpServlet {
//    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        response.setContentType("text/html");
//        PrintWriter out = response.getWriter();
//        try {
//            Class.forName("org.postgresql.Driver");
//            out.println("<p>Driver loaded successfully.</p>");
//            try (Connection connection = DriverManager.getConnection(
//                    "jdbc:postgresql://localhost:5432/abb_tech", "postgres", "mypassword")) {
//                out.println("<p>Connected to the database.</p>");
//            }
//        } catch (ClassNotFoundException | SQLException e) {
//            e.printStackTrace(out);
//        }
//    }
//}