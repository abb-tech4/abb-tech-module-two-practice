package org.abbtech.lesson3;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CalculationRepository {
    private static final String JDBC_URL = "jdbc:postgresql://localhost:5432/abb_tech";
    private static final String USER_NAME = "postgres";
    private static final String PASS = "apricity78";

    public void saveCalculationResult(int a, int b, int result, String method) throws SQLException {
        Connection connection = null;
        String sqlInsert = """
                INSERT INTO "calculation".calculation_result(id, variable_a, variable_b, calc_result, calc_method)
                VALUES (?,?,?,?,?)
                """;

        try {
            String message = "I am here";
            System.out.println(message);
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        try {
            connection = DriverManager.getConnection(JDBC_URL, USER_NAME, PASS);
            connection.setAutoCommit(false);
            PreparedStatement preparedStatement = connection.prepareStatement(sqlInsert);
            preparedStatement.setInt(2, a);
            preparedStatement.setInt(3, b);
            preparedStatement.setInt(4, result);
            preparedStatement.setString(5, method);
            preparedStatement.execute();
            connection.commit();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        }
    }

    public void deleteCalculationResult(int id) throws SQLException {
        Connection connection = null;
        String sqlDelete = """
                DELETE FROM "calculation".calculation_result WHERE id = ?;
                """;

        try {
            String message = "I am here";
            System.out.println(message);
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        try {
            connection = DriverManager.getConnection(JDBC_URL, USER_NAME, PASS);
            connection.setAutoCommit(false);
            PreparedStatement preparedStatement = connection.prepareStatement(sqlDelete);
            preparedStatement.setInt(1, id);
            preparedStatement.execute();
            connection.commit();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        } finally {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        }
    }

    public List<CalculationResultDTO> getCalculationResult() throws SQLException {
        Connection connection = null;
        List<CalculationResultDTO> calculationResultDTOS = new ArrayList<>();
        String sqlSelect = """
                SELECT * FROM "calculation".calculation_result;
                """;

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        try {
            connection = DriverManager.getConnection(JDBC_URL, USER_NAME, PASS);
            connection.setAutoCommit(false);
            PreparedStatement preparedStatement = connection.prepareStatement(sqlSelect);
            ResultSet calculationResults = preparedStatement.executeQuery();
            while (calculationResults.next()) {
                int a = calculationResults.getInt("VARIABLE_A");
                int b = calculationResults.getInt("VARIABLE_B");
                int result = calculationResults.getInt("CALC_RESULT");
                String calcMethod = calculationResults.getString("CALC_METHOD");
                calculationResultDTOS.add(new CalculationResultDTO(a, b, result, calcMethod));
            }
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        } finally {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        }

        return calculationResultDTOS;
    }

    public void updateCalculationResult(int a, int b, int id) throws SQLException {
        Connection connection = null;
        String sqlUpdate = """
                update "calculation".calculation_result set VARIABLE_A = ?, VARIABLE_B = ? WHERE id = ?;
                """;

        try {
            String message = "I am here";
            System.out.println(message);
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        try {
            connection = DriverManager.getConnection(JDBC_URL, USER_NAME, PASS);
            connection.setAutoCommit(false);
            PreparedStatement preparedStatement = connection.prepareStatement(sqlUpdate);
            preparedStatement.setInt(1, a);
            preparedStatement.setInt(2, b);
            preparedStatement.setInt(3, id);
            preparedStatement.execute();
            connection.commit();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        } finally {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        }
    }
}
