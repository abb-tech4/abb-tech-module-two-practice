package org.abbtech.exercise13;

public interface UserService {
    boolean isUserActive(String username);
    void deleteUser(Long userId) throws UserNotFoundException;
    User getUser(Long userId) throws UserNotFoundException;
}