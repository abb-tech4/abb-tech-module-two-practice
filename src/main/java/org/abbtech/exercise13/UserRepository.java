//package org.abbtech.exercise13;
//
//import java.util.List;
//
//public class UserRepository {
//    private final List<User> users;
//
//    public UserRepository(List<User> users) {
//        this.users = users;
//    }
//
//    public User findByUsername(String username) {
//        for (User u : users) {
//            if (u.getName().equals(username)) {
//                return u;
//            }
//        }
//
//        return null;
//    }
//
//    public User findUserId(Long userId) {
//        return users.stream()
//                .filter(u -> userId.equals(u.getUserId()))
//                .findFirst()
//                .orElse(null);
//    }
//}
