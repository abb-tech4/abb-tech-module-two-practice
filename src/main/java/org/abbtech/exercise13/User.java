package org.abbtech.exercise13;

public class User {
    private long userId;
    private String name;
    private boolean isActive;

    public User(long userId, String name, boolean isActive) {
        this.userId = userId;
        this.name = name;
        this.isActive = isActive;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
