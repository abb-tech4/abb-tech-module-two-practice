//package org.abbtech.exercise13;
//
//public class UserServiceImp implements UserService {
//    private final UserRepository userRepository;
//
//    public UserServiceImp(UserRepository userRepository) {
//        this.userRepository = userRepository;
//    }
//
//    public boolean isUserActive(String username) {
//        User user = userRepository.findByUsername(username);
//        return user != null && user.isActive();
//    }
//
//    public void deleteUser(Long userId) throws UserNotFoundException {
//        User user = userRepository.findUserId(userId);
//        if (user==null) {
//            throw new UserNotFoundException("User not found, so could not be deleted");
//        }
//    }
//
//    public User getUser(Long userId) throws UserNotFoundException {
//        User user = userRepository.findUserId(userId);
//        if (user==null){
//            throw new UserNotFoundException("User not found, so could not be retrieved");
//        }
//        return user;
//    }
//}
