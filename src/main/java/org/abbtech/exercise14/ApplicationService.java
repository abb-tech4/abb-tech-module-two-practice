package org.abbtech.exercise14;

public class ApplicationService {
    private final CalculatorService calculatorService;

    public ApplicationService(CalculatorService calculatorService) {
        this.calculatorService = calculatorService;
    }

    public double addition(double a, double b) {
        if (a <= 0 || b <= 0) {
            throw new ArithmeticException("a and b should be positive.");
        }

        System.out.println("check");

        return calculatorService.addition(a, b);
    }

    public double subtract(double a, double b) {
        var result = calculatorService.subtract(a, b);

        if (result == 2 || result == 4 || result == 6) {
            throw new ArithmeticException("2, or 4, or 6 as a result are not allowed.");
        }

        return result;
    }

    public double multiply(double a, double b) {
        if (a > 4 && b > 6) {
            throw new ArithmeticException("a should not be greater than 4 and b should not be greater than 6.");
        }

        return calculatorService.multiply(a, b);
    }

    public double division(double a, double b) {
        if (a % 2 != 0 || b % 2 != 0) {
            throw new ArithmeticException("a and b should be even.");
        }

        if (b == 0) {
            throw new ArithmeticException("Division by 0 is not possible.");
        }

        return calculatorService.division(a, b);
    }
}