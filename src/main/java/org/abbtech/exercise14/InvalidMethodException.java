package org.abbtech.exercise14;

public class InvalidMethodException extends Exception {
    public InvalidMethodException(String message) {
        super(message);
    }
}
