//package org.abbtech.exercise14;
//
//import jakarta.servlet.ServletException;
//import jakarta.servlet.ServletRequest;
//import jakarta.servlet.ServletResponse;
//import jakarta.servlet.annotation.WebServlet;
//import jakarta.servlet.http.HttpServlet;
//import jakarta.servlet.http.HttpServletRequest;
//import jakarta.servlet.http.HttpServletResponse;
//
//import java.io.BufferedReader;
//import java.io.IOException;
//import java.io.PrintWriter;
//
//@WebServlet(name = "CalculationServlet", urlPatterns = {"/calculator/add", "/calculator/subtract", "/calculator/multiply", "/calculator/division"})
//public class CalculationServlet extends HttpServlet {
//    private transient ApplicationService applicationService;
//
//    @Override
//    public void init() throws ServletException {
//        System.out.println("init() method called");
//        super.init();
//        applicationService = new ApplicationService(new CalculatorServiceImpl());
//    }
//
//    @Override
//    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
//        System.out.println("service() method called");
//        super.service(req, res);
//    }
//
//    @Override
//    public void destroy() {
//        System.out.println("destroy() method called");
//        super.destroy();
//    }
//
//    @Override
//    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
//        String calculationMethod = req.getHeader("x-calculation-method");
//        double a = Double.parseDouble(req.getParameter("a"));
//        double b = Double.parseDouble(req.getParameter("b"));
//        double result;
//
//        PrintWriter writer = resp.getWriter();
//
//        try {
//            if (calculationMethod == null) {
//                throw new InvalidMethodException("Add x-calculation-method header");
//            }
//
//            result = switch (calculationMethod.toLowerCase()) {
//                case "add" -> applicationService.addition(a, b);
//                case "subtract" -> applicationService.subtract(a, b);
//                case "multiply" -> applicationService.multiply(a, b);
//                case "division" -> applicationService.division(a, b);
//                default -> throw new InvalidMethodException("Only add, subtract, multiply, and division methods are accepted.");
//            };
//
//            resp.setContentType("application/json");
//            writer.write(normalResponse(result));
//        } catch (ArithmeticException | InvalidMethodException e) {
//            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
//            writer.write(exceptionResponse(e.getMessage()));
//        } finally {
//            writer.close();
//        }
//    }
//
//    @Override
//    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws IOException {
//        String sign = req.getParameter("sign");
//        double result = Double.parseDouble(req.getParameter("result"));
//        PrintWriter writer = resp.getWriter();
//
//        try {
//            result = switch (sign.toLowerCase()) {
//                case "p" -> Math.sqrt(Math.pow(result, 2));
//                case "n" -> (-1) * Math.sqrt(Math.pow(result, 2));
//                default -> throw new ArithmeticException("Sign should be either positive[p] or negative[n].");
//            };
//
//            resp.setContentType("application/json");
//            writer.write(normalResponse(result));
//        } catch (ArithmeticException e) {
//            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
//            writer.write(exceptionResponse(e.getMessage()));
//        } finally {
//            writer.close();
//        }
//    }
//
//    @Override
//    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
//        BufferedReader bufferedReader = req.getReader();
//        PrintWriter writer = resp.getWriter();
//        int lineLine;
//
//        while ((lineLine = bufferedReader.read()) != -1) {
//            char chr = (char) lineLine;
//            writer.write(chr);
//        }
//
//        writer.close();
//    }
//
//    @Override
//    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws IOException {
//        PrintWriter writer = resp.getWriter();
//
//        try {
//            throw new ServletException("Delete method is not supported.");
//        } catch (ServletException e) {
//            writer.write(exceptionResponse(e.getMessage()));
//        } finally {
//            writer.close();
//        }
//
//    }
//
//    String normalResponse(double result) {
//        return """
//                    {
//                        "result":""" + result + """
//                    }
//               """;
//    }
//
//    String exceptionResponse(String message) {
//        return """
//                {
//                    "exception": "%s"
//                }
//                """.formatted(message);
//    }
//}