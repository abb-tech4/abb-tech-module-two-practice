//package org.abbtech.lesson4;
//
//import jakarta.servlet.*;
//import jakarta.servlet.annotation.WebFilter;
//import jakarta.servlet.http.Cookie;
//import jakarta.servlet.http.HttpServletRequest;
//import jakarta.servlet.http.HttpServletResponse;
//import jakarta.servlet.http.HttpSession;
//
//import java.io.IOException;
//
//@WebFilter(filterName = "AuthenticationFilter", urlPatterns = "/user/profile")
//public class AuthenticationFilter implements Filter {
//    @Override
//    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
//        System.out.println("log info");
//        HttpServletRequest req = (HttpServletRequest) request;
//        HttpServletResponse resp = (HttpServletResponse) response;
//        Cookie userCookie = SecurityUtil.getCookieByName(req.getCookies(), "username");
//        HttpSession httpSession = SecurityUtil.getSessionByAttributeName(req.getSession(true),
//                "username");
//        if (userCookie != null && httpSession != null) {
//            chain.doFilter(request, response);
//        } else {
//            resp.setStatus(401);
//            resp.getWriter().write("You are not logged in");
//        }
//    }
//}
