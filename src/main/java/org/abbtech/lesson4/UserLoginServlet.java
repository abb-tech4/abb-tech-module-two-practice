//package org.abbtech.lesson4;
//
//import jakarta.servlet.ServletException;
//import jakarta.servlet.ServletRequest;
//import jakarta.servlet.ServletResponse;
//import jakarta.servlet.annotation.WebServlet;
//import jakarta.servlet.http.*;
//
//import java.io.IOException;
//
//@WebServlet(name = "UserLoginServlet", urlPatterns = {"/user/login"})
//public class UserLoginServlet extends HttpServlet {
//    @Override
//    public void init() throws ServletException {
//        super.init();
//    }
//
//    @Override
//    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
//        super.service(req, res);
//    }
//
//    @Override
//    public void destroy() {
//        super.destroy();
//    }
//
//    @Override
//    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        String username = "username";
//        String password = "pass";
//        var writer = resp.getWriter();
//        String actualUsername = req.getParameter("user");
//        String actualPass = req.getParameter("pass");
//        if (username.equalsIgnoreCase(actualUsername) && password.equals(actualPass)) {
//            HttpSession httpSession = req.getSession();
//            httpSession.setAttribute("username", actualUsername);
//            httpSession.setAttribute("user-role", "profile-role");
//            Cookie cookieUser = new Cookie("username", actualUsername);
//            Cookie cookieUserRole = new Cookie("user-role", "profile-role");
//            cookieUser.setMaxAge(300);
//            cookieUserRole.setMaxAge(300);
//            resp.addCookie(cookieUser);
//            resp.addCookie(cookieUserRole);
//            writer.write("User logged in  with username : " + actualUsername);
//        }
//    }
//}