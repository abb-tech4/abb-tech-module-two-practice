//package org.abbtech.lesson4;
//
//import jakarta.servlet.ServletException;
//import jakarta.servlet.ServletRequest;
//import jakarta.servlet.ServletResponse;
//import jakarta.servlet.annotation.WebServlet;
//import jakarta.servlet.http.HttpServlet;
//import jakarta.servlet.http.HttpServletRequest;
//import jakarta.servlet.http.HttpServletResponse;
//import org.abbtech.exam.SecurityUtil;
//
//import java.io.IOException;
//
//@WebServlet(name = "UserProfileServlet", urlPatterns = {"/user/profile"})
//public class UserProfileServlet extends HttpServlet {
//    @Override
//    public void init() throws ServletException {
//        super.init();
//    }
//
//    @Override
//    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
//        super.service(req, res);
//    }
//
//    @Override
//    public void destroy() {
//        super.destroy();
//    }
//
//    @Override
//    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
//        var writer = resp.getWriter();
//        String username = SecurityUtil.getCookieByName(req.getCookies(), "username").getValue();
//        writer.write("User Profile :" + username);
//    }
//}