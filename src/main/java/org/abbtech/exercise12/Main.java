package org.abbtech.exercise12;

public class Main {
    public static void main(String[] args) {
        System.out.println("Addition: " + Calculator.add(6, 1.2));
        System.out.println("Subtraction: " + Calculator.subtract(6, 1.2));
        System.out.println("Multiplication: " + Calculator.multiply(6, 1.2));
        System.out.println("Division: " + Calculator.divide(6, 1.2));
    }
}