//package org.abbtech.exercise13;
//
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.junit.jupiter.MockitoExtension;
//
//import static org.mockito.Mockito.*;
//
//@ExtendWith(MockitoExtension.class)
//class UserServiceImplUnitTest {
//
//    private UserRepository userRepositoryMock;
//    private UserService userService;
//
//    @BeforeEach
//    void setUp() {
//        userRepositoryMock = mock(UserRepository.class);
//        userService = new UserServiceImp(userRepositoryMock);
//    }
//
//    @Test
//    void isUserActiveSuccess() {
//        when(userRepositoryMock.findByUsername("activeUser")).thenReturn(new User(1, "activeUser", true));
//        boolean isActive = userService.isUserActive("activeUser");
//        Assertions.assertTrue(isActive);
//    }
//
//    @Test
//    void isUserActiveFail() {
//        when(userRepositoryMock.findByUsername("unknownUser")).thenReturn(null);
//        boolean isActive = userService.isUserActive("unknownUser");
//        Assertions.assertFalse(isActive);
//    }
//
//    @Test
//    void deleteUserSuccess() {
//        when(userRepositoryMock.findUserId(1L)).thenReturn(new User(1L, "existingUser", true));
//        Assertions.assertDoesNotThrow(() -> userService.deleteUser(1L));
//    }
//
//    @Test
//    void deleteUserThrows() {
//        when(userRepositoryMock.findUserId(anyLong())).thenReturn(null);
//        Assertions.assertThrows(UserNotFoundException.class, () -> userService.deleteUser(999L));
//    }
//
//    @Test
//    void getUserSuccess() throws Exception {
//        User expectedUser = new User(1L, "existingUser", true);
//        when(userRepositoryMock.findUserId(1L)).thenReturn(expectedUser);
//        User actualUser = userService.getUser(1L);
//
//        Assertions.assertNotNull(actualUser);
//        Assertions.assertEquals(expectedUser.getUserId(), actualUser.getUserId());
//        Assertions.assertEquals(expectedUser.getName(), actualUser.getName());
//        Assertions.assertEquals(expectedUser.isActive(), actualUser.isActive());
//    }
//
//    @Test
//    void getUserThrows() {
//        when(userRepositoryMock.findUserId(anyLong())).thenReturn(null);
//        Assertions.assertThrows(UserNotFoundException.class, () -> userService.getUser(999L));
//    }
//}