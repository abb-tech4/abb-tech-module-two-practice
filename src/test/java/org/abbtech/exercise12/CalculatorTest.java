//package org.abbtech.exercise12;
//
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//
//import static org.hamcrest.MatcherAssert.assertThat;
//import static org.hamcrest.Matchers.*;
//
//public class CalculatorTest {
//    double[] firstInputs;
//    double[] secondInputs;
//    double actualResult;
//
//    // just for showing the use of "BeforeEach"
//    @BeforeEach
//    void init() {
//        firstInputs = new double[]{55, -10, 45.6, 100000000, 5.66};
//        secondInputs = new double[]{-100, 30, 52, 99999, 0};
//        actualResult = 0;
//    }
//
//    @Test
//    void addTest() {
//        Calculator calculator = new Calculator();
//        Calculator calculator1 = null;
//        double actualResult = Calculator.add(firstInputs[0], secondInputs[0]);
//
//        Assertions.assertEquals(actualResult, -45); // successful
//        Assertions.assertTrue(actualResult < 0); // successful
////        Assertions.assertFalse(actualResult * 10 < 0); // fail
//        Assertions.assertNull(calculator1); // always null, successful
//        Assertions.assertNotNull(calculator); // always not null, successful
////        Assertions.assertArrayEquals(firstInputs, secondInputs); // fail
////        Assertions.assertSame(calculator, calculator1); // fail
//        Assertions.assertNotSame(calculator, calculator1); // successful
//
//        // there are much more such as is(not()), either().or(), etc.
////        assertThat(actualResult, is(-45.0)); // successful
//    }
//
//    @Test
//    void subtractTest() {
//        actualResult = Calculator.subtract(firstInputs[1], secondInputs[1]);
////        Assertions.assertTrue(actualResult >= 0); // failed
//
////        assertThat(actualResult, is(-40)); // failed
//        assertThat(actualResult, is(-40.0)); // success
//    }
//
//    @Test
//    void multiplyTest() {
//        actualResult = Calculator.multiply(firstInputs[2], secondInputs[2]);
//
////        Assertions.assertSame(actualResult, (45.6 * 52)); // fail
//        Assertions.assertTrue(actualResult == (45.6 * 52)); // success
//    }
//
//    @Test
//    void divideTest() {
//        actualResult = Calculator.divide(firstInputs[3], secondInputs[3]);
//
////        Assertions.assertEquals(actualResult, (100000000 / 9999)); // fail
//
//        actualResult = Calculator.divide(firstInputs[4], secondInputs[4]);
////        Assertions.assertEquals(actualResult, (5.66 / 0)); // fail
//
//        actualResult = Calculator.divide(secondInputs[4], firstInputs[4]);
//        Assertions.assertEquals(actualResult, (0)); // success
//    }
//}
