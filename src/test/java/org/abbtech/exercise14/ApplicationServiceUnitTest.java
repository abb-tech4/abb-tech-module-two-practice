package org.abbtech.exercise14;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ApplicationServiceUnitTest {
    @Mock
    private CalculatorServiceImpl calculatorService;

    @InjectMocks
    private ApplicationService applicationService;

    @Test
    void addition_success() {
        when(calculatorService.addition(1, 2)).thenReturn(3.0);
        double result = applicationService.addition(1, 2);
        verify(calculatorService, times(1)).addition(1, 2);
        Assertions.assertEquals(3.0, result);
    }

    @ParameterizedTest
    @CsvSource(value = {"-1,1", "1,-1", "0,1", "1,0"}, delimiter = ',')
    void addition_throws(double a, double b) {
        Exception exception = Assertions.assertThrows(ArithmeticException.class, () -> applicationService.addition(a, b));
        Assertions.assertEquals("a and b should be positive.", exception.getMessage());
    }

    @Test
    void subtract_success() {
        when(calculatorService.subtract(10, 2)).thenReturn(8.0);
        double result = applicationService.subtract(10, 2);
        verify(calculatorService, times(1)).subtract(10, 2);
        Assertions.assertEquals(8.0, result);
    }

    @ParameterizedTest
    @CsvSource(value = {"8,6", "10,6", "12,6"}, delimiter = ',')
    void subtract_throws(double a, double b) {
        when(calculatorService.subtract(a, b)).thenReturn(2.0, 4.0, 6.0);
        Exception exception = Assertions.assertThrows(ArithmeticException.class, () -> applicationService.subtract(a, b));
        Assertions.assertEquals("2, or 4, or 6 as a result are not allowed.", exception.getMessage());
    }

    @Test
    void multiply_success() {
        when(calculatorService.multiply(2, 3)).thenReturn(6.0);
        double result = applicationService.multiply(2, 3);
        verify(calculatorService, times(1)).multiply(2, 3);
        Assertions.assertEquals(6.0, result);
    }

    @ParameterizedTest
    @CsvSource(value = {"5,7", "5,8", "6,7"}, delimiter = ',')
    void multiply_throws(double a, double b) {
        Exception exception = Assertions.assertThrows(ArithmeticException.class, () -> applicationService.multiply(a, b));
        Assertions.assertEquals("a should not be greater than 4 and b should not be greater than 6.", exception.getMessage());
    }

    @Test
    void division_success() {
        when(calculatorService.division(4, 2)).thenReturn(2.0);
        double result = applicationService.division(4, 2);
        verify(calculatorService, times(1)).division(4, 2);
        Assertions.assertEquals(2.0, result);
    }

    @ParameterizedTest
    @CsvSource(value = {"3,2", "4,3", "3,0"}, delimiter = ',')
    void division_throws_odd(double a, double b) {
        Exception exception = Assertions.assertThrows(ArithmeticException.class, () -> applicationService.division(a, b));
        Assertions.assertEquals("a and b should be even.", exception.getMessage());
    }

    @ParameterizedTest
    @CsvSource(value = {"4,0", "6,0", "8,0"}, delimiter = ',')
    void division_throws_zero(double a, double b) {
        Exception exception = Assertions.assertThrows(ArithmeticException.class, () -> applicationService.division(a, b));
        Assertions.assertEquals("Division by 0 is not possible.", exception.getMessage());
    }
}